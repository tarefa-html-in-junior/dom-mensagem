const entradaMensagem = document.getElementById('entradaMensagem');
const formulario = document.querySelector('form');
const listaMensagens = document.getElementById('listaMensagens');

function enviarMensagem(evento) {
    evento.preventDefault();
    const mensagem = entradaMensagem.value.trim();
    if (mensagem !== '') {
        adicionarMensagem(mensagem);
        entradaMensagem.value = '';
    }
}

function adicionarMensagem(mensagem) {
    const elementoMensagem = document.createElement('div');
    elementoMensagem.textContent = mensagem;
    listaMensagens.appendChild(elementoMensagem);
}

formulario.addEventListener('submit', enviarMensagem);
